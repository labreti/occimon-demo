# How to use this demo #

The purpose of the demo is to show how the information contained in the description of sensor and collector OCCI-entities is used to dynamically create and operate a simple monitoring infrastructure that periodically checks network connectivity and CPU load of a compute resource.

To run the demo you need to have VirtualBox and Vagrant installed on your PC. It should run on any OS.

This demo creates a virtual network with three Virtual Machines:

* webserver (192.168.5.2): makes available the description of the OCCI resources
* compute  (192.168.5.3): a compute resource that participates to the monitoring activity
* sensor (192.168.5.4): a sensor resource that controls the monitoring activity

## Launching the demo ##

The three VMs are launched with the command

```
#!bash

$ vagrant up
```
in the directory where you clone this repository. The following commands refer to the same directory.

### Start the web server ###

In a terminal type the following commands

```
#!bash

$ vagrant ssh webserver
vagrant@webserver:$ cd /vagrant
vagrant@webserver:/vagrant$ ./httpServer.py 
Server pronto...

```
The server is now ready to provide the description of the OCCI entities contained in the files 

* urn:uuid:2345 : a collector
* urn:uuid:c2222 : a collector link
* urn:uuid:s1111 : a sensor resource

### Start the compute resource ###

In another terminal type the following commands

```
#!bash

$ vagrant ssh compute
vagrant@compute:~$ java -jar /vagrant/MetricContainer-v2.jar urn:uuid:c2222 http://192.168.5.2:6789/
Launch collector endpoint http://192.168.5.2:6789/urn:uuid:c2222
Metric container is ready (192.168.5.3:12312)
Now collecting IsReachable
Now collecting CPUPercent

```
The first parameter indicates the location of OCCI description of the metric collector, the second parameter indicates the web server where the description is maintained.

The metric collector, as described in the urn:uuid:c2222, collects two metrics: the reachability of the host at 192.168.5.2 and the local CPU load.

### Start the sensor resource ###

In another terminal type the following commands 
```
#!bash

$ vagrant ssh sensor
vagrant@sensor:~$ java -jar /vagrant/Demo-v2.jar urn:uuid:s1111 http://192.168.5.2:6789/
Launching sensor urn:uuid:s1111
Sensor receiving from TCP socket 192.168.5.4:37900
Launching remote collectors: [urn:uuid:2345]
Sensor launching collector from 192.168.5.3:12312
Logging true to my/log/file
ewma input: 0.0
sendudp: sending 0.0
...

```
Again, the first parameter is the description of the OCCI sensor, the second parameter is the webserver where the description is located.

The sensor outputs to the stdout the data received from the collector, and the output form the EWMA mixin.

### Inspect monitoring output ###

According with the description of the sensor, the information is delivered to the UDP address 192.168.5.1:8888. To observe the output of the monitoring infrastructure give the following command in the host machine:


```
#!bash

$ nc -ul 8888
Data: 0.0
Data: 0.0
...
```
To obtain an output different from 0.0, open another ssh terminal on the compute VM and submit a command qith non negligible footprint, like

vagrant@compute:~$ grep pippo --recursive /usr"

![Schermata del 2015-03-26 18:33:07.png](https://bitbucket.org/repo/rygqaM/images/3990519683-Schermata%20del%202015-03-26%2018:33:07.png)